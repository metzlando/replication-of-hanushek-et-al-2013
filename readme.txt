This repository contains a replication of the paper "Does school autonomy make sense everywhere? Panel estimates from PISA"
by Hanushek, E. A., Link, S., Woessmann, L. (2013). It was elaborated in the Master's degree class "Econometrics",
which was part of the EPOG-program (Economic Policies in the Age of Globalization; www.epog.eu) in the winter term 2018/19
at Université Paris 13 (Paris Nord) and Université Paris Diderot (Paris 7). The programming language used is R.
The repository contains the following files:

* the "readme.txt"
* the code of the replication "1- Imputation and replication_new_presentation.R"
* The final paper "Metzler_Paper_Econometrics_EPOG_2019.pdf"
* The original paper "Does-school-autonomy-make-sense-everywhere-Pane_2013_Journal-of-Development.pdf"
* The dataset which was used "pisa_final.csv"

The code allows the full replication of the obtained results stated in the final paper "Metzler_Paper_Econometrics_EPOG_2019.pdf".
For the paper, it was of particular interest to achieve the same result as the original paper. This regards to graphs, tables and regressions.
Due to problems in the methodology of the original paper - which only became apparent during the working process - the main results could not be confirmed.